@extends('layout.master')

@section('judul')
Halaman Form
@endsection
@section('content') 
    <form action="/welcome" method="post">
        @csrf
        <label>Nama lengkap</label> <br><br>
        <input type="text" name="nama"><br><br>
        <label >Alamat</label><br><br>
        <textarea name="address" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">
    </form>
@endsection